import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaludadorComponent } from './saludador/saludador.component';

@NgModule({ // Decorador
  declarations: [ // Para cargar los componentes pipes y directivas
    AppComponent,
    SaludadorComponent
  ],
  imports: [ // Para cargar modulos
    BrowserModule,
    AppRoutingModule
  ],
  providers: [], // Para cargar servicios
  bootstrap: [AppComponent] //Cargar componente principal
})
export class AppModule { } //Exportar modulo
