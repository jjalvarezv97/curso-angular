import { Component } from '@angular/core'; //Aca importamos el compontente que esta guardado en el core

@Component({ //Decorador
  selector: 'app-root', // Etiqueta o directiva que se puede cargar
  templateUrl: './app.component.html', // Define el template o la vista
  styleUrls: ['./app.component.css']  // Define los estilos
})
export class AppComponent { // export para poder usar el componente en otras partes
  title = 'angular-hola-mundo'; //Propiedad 
}
