
export class Destino{

	private selected : Boolean;
	public nombre : string;
	public url : string;
	public comodidades : string[];

	constructor(nombre:string, url:string){

		this.nombre = nombre;
		this.url = url;
		this.comodidades = ['Cama Doble', 'Turco', 'Desayuno', 'Minibar'];

	}

	public getSelected():Boolean{
		return this.selected;
	}
	public setSelected(valor:boolean){
		this.selected = valor;
	}

}
