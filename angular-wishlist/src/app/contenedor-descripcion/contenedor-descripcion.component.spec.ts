import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorDescripcionComponent } from './contenedor-descripcion.component';

describe('ContenedorDescripcionComponent', () => {
  let component: ContenedorDescripcionComponent;
  let fixture: ComponentFixture<ContenedorDescripcionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContenedorDescripcionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContenedorDescripcionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
