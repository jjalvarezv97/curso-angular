import { Component, OnInit} from '@angular/core';

@Component({
	selector: 'contDescripcion',
	templateUrl: './contenedor-descripcion.component.html',
	styleUrls: ['./contenedor-descripcion.component.css']
})
export class ContenedorDescripcionComponent implements OnInit {

	descripciones : Array<String>;

	constructor() {
		this.descripciones = ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices ac tortor quis ultricies. Phasellus interdum orci felis, vitae maximus lacus iaculis eu. Integer sit amet viverra velit. Praesent maximus dictum faucibus. Nulla viverra libero turpis, ac lacinia turpis tincidunt vitae. Curabitur a turpis gravida, fringilla est et, maximus ante. Aliquam sodales finibus tellus.',
		'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices ac tortor quis ultricies. Phasellus interdum orci felis, vitae maximus lacus iaculis eu. Integer sit amet viverra velit. Praesent maximus dictum faucibus. Nulla viverra libero turpis, ac lacinia turpis tincidunt vitae. Curabitur a turpis gravida, fringilla est et, maximus ante. Aliquam sodales finibus tellus.',
		'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices ac tortor quis ultricies. Phasellus interdum orci felis, vitae maximus lacus iaculis eu. Integer sit amet viverra velit. Praesent maximus dictum faucibus. Nulla viverra libero turpis, ac lacinia turpis tincidunt vitae. Curabitur a turpis gravida, fringilla est et, maximus ante. Aliquam sodales finibus tellus. ']
	}

	ngOnInit(): void {
	}

}
