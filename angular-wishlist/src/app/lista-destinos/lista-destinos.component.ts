import { Component, OnInit } from '@angular/core';
import {Destino} from './../models/destinos.component';

@Component({
	selector: 'lista-destinos',
	templateUrl: './lista-destinos.component.html',
	styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

	//Lista de destinos
	wish : Array<Destino>;

	constructor() {
		this.wish = []
	}

	//Funcion a ejecutar cuando se haga el submit del formulario
	mostrar(nom:string, url:string):boolean{

		//Agrego un nuevo destino a la lista de destinos
		this.wish.push(new Destino(nom,url));
		console.log(this.wish);

		return false; //Para que no se recargue la pagina luego del evento
	}

	elegido(dest : Destino){
		for(let i of this.wish){
			i.setSelected(false);
		}
		dest.setSelected(true);
	}

	ngOnInit(): void {
	}

}
