import { Component, OnInit, Input} from '@angular/core';

@Component({
	selector: 'descripcion',
	templateUrl: './descripcion.component.html',
	styleUrls: ['./descripcion.component.css']
})
export class DescripcionComponent implements OnInit {

	@Input() valorDescripcion :string;

	constructor() {

		this.valorDescripcion = '';

	}

	ngOnInit(): void {
	}

}
