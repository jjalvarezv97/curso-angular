
import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { Destino } from './../models/destinos.component';



@Component({
	selector: 'destino-viaje',
	templateUrl: './destino-viaje.component.html',
	styleUrls: ['./destino-viaje.component.css']
})

export class DestinoViajeComponent implements OnInit {

	@Input() destino: Destino;
	@Input() indiceDestino: number;
	// Accedo a atributos de etiquetas, la variable de acontinuacion solo es para asignar
	@HostBinding('attr.class') c = "col-md-4";
	@Output() emitidor: EventEmitter<Destino>; //Variable de salida con @Output() y le asociamos un Evento tipo Destino

	constructor() {
		this.emitidor = new EventEmitter();
	}

	ngOnInit(): void {
	}

	fnDestacar() {
		this.emitidor.emit(this.destino); //Pasamos un objeto destino al emitidor 
		return false;
	}

}
